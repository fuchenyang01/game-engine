#include "GameObjectFactory.h"

#include "Player.h"
#include "Ground.h"
#include "NPC.h"
#include <cassert>
#include <cstring>
#include"MyCamera.h"

GameObjectFactory::GameObjectFactory()
{

}

GameObjectFactory::~GameObjectFactory()
{

}

/* static */
GameObject* GameObjectFactory::create(const char* pType)
{
	/* The book uses strcmpi here, this appears to be some kind of windows only
	method which I'm avoiding. Strcasecmp is POSIX and BSD standard from 2001
	*/
	if (!strcmpi(pType, "player"))
	{
		return new Player();
	}
	else
	{
		if (!strcmpi(pType, "ground"))
		{
			return new Ground();
		}

		else if (!strcmpi(pType, "NPC"))
		{
			return new NPC();
		}
		
		else
		{
			assert(!"GameObjectFactory::Create(): unknown Type");
		}
	}
}

GameObject * GameObjectFactory::create(const char * pType, SceneManager * scnMgr, RenderWindow * window, SceneNode* node)
{
	if (!strcmpi(pType, "camera"))
	{
		return new MyCamera(scnMgr, window, node);
	}
	else
	{
		assert(!"GameObjectFactory::Create(): unknown Type");
	}
}
