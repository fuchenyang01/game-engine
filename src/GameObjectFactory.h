#pragma once
#include "GameObject.h"


class GameObjectFactory
{
public:
	GameObjectFactory();
	~GameObjectFactory();

	static GameObject* create(const char* pType);
	static GameObject* create(const char* pType, SceneManager* scnMgr, RenderWindow* window, SceneNode* node);
};