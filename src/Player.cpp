#include "Player.h"
#include <iostream>
Player::Player()
{
  node = nullptr;
  entity = nullptr;
  Vector3 meshBoundingBox(0.0f,0.0f,0.0f);

  colShape = nullptr;
  dynamicsWorld = nullptr;
  radian = 0;
  forwardForce = -1000.0f;
  turningForce = 1000.0f;
  linearDamping = 0.2f;
  angularDamping = 0.8f;
  
  
}

Player::~Player()
{

}

void Player::createMesh(SceneManager* scnMgr)
{
  entity = scnMgr->createEntity("ship1.mesh");
 
}

void Player::attachToNode(SceneNode* parent)
{
  node = parent->createChildSceneNode();
  node->attachObject(entity);
  node->setScale(100.0f,100.0f,100.0f);
  boundingBoxFromOgre();
}

void Player::setScale(float x, float y, float z)
{
    node->setScale(x,y,z);
}


void Player::setRotation(Vector3 axis, Radian rads)
{
  //quat from axis angle
  Quaternion quat(rads, axis);
  node->setOrientation(quat);
 
}

void Player::setPosition(float x, float y, float z)
{
  node->setPosition(x,y,z);
}

void Player::boundingBoxFromOgre()
{
  //get bounding box here.
  node->_updateBounds();
  const AxisAlignedBox& b = node->_getWorldAABB();
  Vector3 temp(b.getSize());
  meshBoundingBox = temp;
}

void Player::createRigidBody(float bodyMass)
{
 // colShape = new btBoxShape(btVector3(meshBoundingBox.x/2.0f, meshBoundingBox.y/2.0f, meshBoundingBox.z/2.0f));
  BtOgre::StaticMeshToShapeConverter converter(entity);
  colShape = converter.createBox();


  // Create Dynamic Objects
  btTransform startTransform;
  startTransform.setIdentity();

  Quaternion quat2 = node->_getDerivedOrientation();
  startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

  Vector3 pos = node->_getDerivedPosition();
  startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

  btScalar mass(bodyMass);

  //rigidbody is dynamic if and only if mass is non zero, otherwise static
  bool isDynamic = (mass != 0.f);

  btVector3 localInertia(0, 0, 0);
  if (isDynamic)
  {
      // Debugging
      //std::cout << "I see the cube is dynamic" << std::endl;
      colShape->calculateLocalInertia(mass, localInertia);
  }

  //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
  btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
  btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);

  body = new btRigidBody(rbInfo);

  
  body->setDamping(linearDamping, angularDamping);
  //Set the user pointer to this object.
  body->setUserPointer((void*)this);
}

void Player::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes)
{
  collisionShapes.push_back(colShape);
}

void Player::addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld)
{
  this->dynamicsWorld = dynamicsWorld;
  dynamicsWorld->addRigidBody(body);
}

void Player::setMass(float mass)
{
	this->mass = mass;
}

void Player::update(float dt)
{
  btTransform trans;

  if (body && body->getMotionState())
  {
    body->getMotionState()->getWorldTransform(trans);
    btQuaternion orientation = trans.getRotation();

    node->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
    node->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
  }

}

void Player::setup(SceneManager* scnMgr, btAlignedObjectArray<btCollisionShape*> collisionShapes, btDiscreteDynamicsWorld* dynamicsWorld)
{
	node = scnMgr->getRootSceneNode();
	setMass(3);

	// Axis
	Vector3 axis(0.0, 1.0, 0.0);
	axis.normalise();

	createMesh(scnMgr);
	attachToNode(node);
	Radian rads(radian);
	setRotation(axis, rads);
	setPosition(0.0f, 500, 0.0f);
	

	createRigidBody(mass);
	addToCollisionShapes(collisionShapes);
	addToDynamicsWorld(dynamicsWorld);

	dbgdraw = new BtOgre::DebugDrawer(scnMgr->getRootSceneNode(), dynamicsWorld);
	dynamicsWorld->setDebugDrawer(dbgdraw);
	dbgdraw->setDebugMode(bulletDebugDraw);
}

SceneNode * Player::getNode()
{
	return node;
}

btRigidBody * Player::getBody()
{
	return body;
}

void Player::forward()
{
	//Create a vector in local coordinates
	//pointing down z.
	btVector3 fwd(0.0f, 0.0f, forwardForce);
	btVector3 push;

	btTransform trans;

	if (body && body->getMotionState())
	{
		//get the orientation of the rigid body in world space.
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		//rotate the local force, into the global space.
		//i.e. push in down the local z.
		push = quatRotate(orientation, fwd);

		//activate the body, this is essential if the body
		//has gone to sleep (i.e. stopped moving/colliding).
		body->activate();

		//apply a force to the center of the body
		body->applyCentralForce(push);
	}
	
}

void Player::back()
{
	//Create a vector in local coordinates
	//pointing down z.
	btVector3 fwd(0.0f, 0.0f, -forwardForce);
	btVector3 push;

	btTransform trans;

	if (body && body->getMotionState())
	{
		//get the orientation of the rigid body in world space.
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		//rotate the local force, into the global space.
		//i.e. push in down the local z.
		push = quatRotate(orientation, fwd);

		//activate the body, this is essential if the body
		//has gone to sleep (i.e. stopped moving/colliding).
		body->activate();

		//apply a force to the center of the body
		body->applyCentralForce(push);
	}
}

void Player::right()
{
	//Apply a turning force to the front of the body.
	//this is an axis (around which to turn)
	//lenght of the vector is the magnitue of the torque.
	btVector3 right(0.0f, -1000.0f, 0.0f);
	btVector3 turn;

	btTransform trans;

	if (body && body->getMotionState())
	{
		//again get the orientation of the body.
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		//orientated the local force into world space.
		turn = quatRotate(orientation, right);

		//activate the body, this is essential if the body
		//has gone to sleep (i.e. stopped moving/colliding).
		body->activate();

		//body->applyTorque(right);
		body->applyTorqueImpulse(right);
	}
	

}

void Player::left()
{
	//Apply a turning force to the front of the body.
	//this is an axis (around which to turn)
	//lenght of the vector is the magnitue of the torque.
	btVector3 left(0.0f, 1000.0f, 0.0f);
	btVector3 turn;

	btTransform trans;

	if (body && body->getMotionState())
	{
		//again get the orientation of the body.
		body->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		//orientated the local force into world space.
		turn = quatRotate(orientation, left);

		//activate the body, this is essential if the body
		//has gone to sleep (i.e. stopped moving/colliding).
		body->activate();

		//body->applyTorque(left);
		body->applyTorqueImpulse(left);

	}
	
}

void Player::stop()
{
	body->activate(false);
}
