#pragma once

/* Ogre3d Graphics*/
#include "Ogre.h"

/* Bullet3 Physics */
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

/* BtOgre's debug draw */
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"

using namespace Ogre;
class GameObject
{
protected:
	SceneNode* node;     /**< Scene graph node */
	Entity* entity;                 /**< Mesh entity */
	Vector3 meshBoundingBox;     /**< Size of the bounding mesh from ogre */

	btCollisionShape* colShape;  /**< Collision shape, describes the collision boundary */
	btRigidBody* body;           /**< Rigid Body */
	
public:
	//Debugging.
	BtOgre::DebugDrawer *dbgdraw;
	//Debugging flags.
	bool ogreDebugDraw;
	bool bulletDebugDraw;
	btDiscreteDynamicsWorld * dynamicsWorld;  /**< physics/collision world */
	GameObject();
	virtual ~GameObject();

	/* Pure Virtuals */
	
	virtual void update(float dt) = 0;
	virtual void setup(SceneManager* scnMgr, btAlignedObjectArray<btCollisionShape*> collisionShapes, btDiscreteDynamicsWorld* dynamicsWorld) = 0;
	virtual SceneNode* getNode() = 0;
	virtual btRigidBody* getBody() = 0;
	virtual void setRotation(Vector3 axis, Radian angle) = 0;

};