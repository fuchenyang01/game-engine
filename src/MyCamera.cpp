#include "MyCamera.h"



MyCamera::MyCamera(SceneManager* scnMgr, RenderWindow* window,SceneNode* node) : spd(10),
turn_spd(12),
pitch_spd(4),
cam_node(0),
pitch_node(0)
{
	cam = scnMgr->createCamera("UserCamera");
	cam->setNearClipDistance(5);

	cam_node = node->createChildSceneNode();
	cam_node->setPosition(0,1, 6);	
	pitch_node = cam_node->createChildSceneNode();
	//pitch_node->lookAt(Vector3(-1,-1, -1), Node::TransformSpace::TS_WORLD);
	pitch_node->attachObject(cam);

	initViewports(window);
}


MyCamera::~MyCamera()
{
}

void MyCamera::update(float dt)
{
	/*Ogre::Vector3 movement(0, 0, 0);
	Ogre::Vector3 direction = node->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z;
	direction.normalise();
	const Input input = GameManager::instance().input_system->input;

	if (input.up)
		movement += direction;
	if (input.down)
		movement -= direction;

	if (input.right)
	{
		movement.x -= direction.z;
		movement.z += direction.x;
	}
	if (input.left)
	{
		movement.x += direction.z;
		movement.z -= direction.x;
	}
	movement.normalise();
	node->translate(dt * spd * movement);
	node->yaw(Ogre::Degree(dt * -input.rot_x * turn_spd));
	pitch_node->pitch(Ogre::Degree(dt * -input.rot_y * pitch_spd));*/
}

void MyCamera::setup(SceneManager * scnMgr, btAlignedObjectArray<btCollisionShape*> collisionShapes, btDiscreteDynamicsWorld * dynamicsWorld)
{
}

btRigidBody * MyCamera::getBody()
{
	return nullptr;
}

void MyCamera::setRotation(Vector3 axis, Radian angle)
{
}

void MyCamera::initViewports(RenderWindow* window)
{
	Ogre::Viewport* vp = window->addViewport(cam);
	vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

	cam->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}
