#pragma once
#include <OgreCamera.h>
#include <OgreSceneNode.h>
#include "Ogre.h"
#include "Game.h"
using namespace Ogre;

class MyCamera :public GameObject
{
public:
	MyCamera(SceneManager* scnMgr, RenderWindow* window, SceneNode* node);
	virtual ~MyCamera();
	void update(float dt);

	Ogre::SceneNode* getNode() { return node; }
	Ogre::Camera* getOgreCamera() { return cam; }
	void setup(SceneManager* scnMgr, btAlignedObjectArray<btCollisionShape*> collisionShapes, btDiscreteDynamicsWorld* dynamicsWorld);
	btRigidBody* getBody();
	void setRotation(Vector3 axis, Radian angle);
private:
	void initViewports(RenderWindow* window);

	float spd, turn_spd, pitch_spd;

	Ogre::Camera* cam;
	Ogre::SceneNode* cam_node;
	Ogre::SceneNode* pitch_node;
};

