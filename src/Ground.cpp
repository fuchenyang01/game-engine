#include "Ground.h"



Ground::Ground()
{
	radian = 0;
	node = nullptr;
	entity = nullptr;
	Vector3 meshBoundingBox(0.0f, 0.0f, 0.0f);

	colShape = nullptr;
	dynamicsWorld = nullptr;
}


Ground::~Ground()
{
}

void Ground::createMesh(SceneManager * scnMgr)
{
	entity = scnMgr->createEntity("map.mesh");
	
}

void Ground::attachToNode(SceneNode * parent)
{
	node = parent->createChildSceneNode();
	node->attachObject(entity);
	node->setScale(100.0f, 100.0f, 100.0f);
	boundingBoxFromOgre();
}

void Ground::setScale(float x, float y, float z)
{
	node->setScale(x, y, z);
}

void Ground::setRotation(Vector3 axis, Radian rads)
{

}

void Ground::setPosition(float x, float y, float z)
{
	node->setPosition(x, y, z);
}

void Ground::boundingBoxFromOgre()
{
	//get bounding box here.
	node->_updateBounds();
	const AxisAlignedBox& b = node->_getWorldAABB();
	Vector3 temp(b.getSize());
	meshBoundingBox = temp;
}

void Ground::createRigidBody(float bodyMass)
{
	//colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2.0f, meshBoundingBox.y / 2.0f, meshBoundingBox.z / 2.0f));
	BtOgre::StaticMeshToShapeConverter converter(entity);
	colShape = converter.createTrimesh();


	// Create Dynamic Objects
	btTransform startTransform;
	startTransform.setIdentity();

	Quaternion quat2 = node->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

	Vector3 pos = node->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	btScalar mass(bodyMass);

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
	{
		// Debugging
		//std::cout << "I see the cube is dynamic" << std::endl;
		colShape->calculateLocalInertia(mass, localInertia);
	}

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);

	body = new btRigidBody(rbInfo);
	 
	//Set the user pointer to this object.
	//body->setUserPointer((void*)this);
	
}

void Ground::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*>& collisionShapes)
{
	collisionShapes.push_back(colShape);
}

void Ground::addToDynamicsWorld(btDiscreteDynamicsWorld * dynamicsWorld)
{
	this->dynamicsWorld = dynamicsWorld;
	dynamicsWorld->addRigidBody(body);
}

void Ground::setMass(float mass)
{
	this->mass = mass;
}

void Ground::update(float dt)
{
	
}

void Ground::setup(SceneManager * scnMgr, btAlignedObjectArray<btCollisionShape*> collisionShapes, btDiscreteDynamicsWorld * dynamicsWorld)
{
	node = scnMgr->getRootSceneNode();
	setMass(0);
	
	// Axis
	Vector3 axis(0.0, 1.0, 0.0);
	axis.normalise();

	createMesh(scnMgr);
	attachToNode(node);
	Radian rads(radian);
	setRotation(axis, rads);
	setPosition(0.0f, 0.0f, 0.0f);


	createRigidBody(mass);
	addToCollisionShapes(collisionShapes);
	addToDynamicsWorld(dynamicsWorld);

	entity->setMaterialName("Examples/Rockwall");

	dbgdraw = new BtOgre::DebugDrawer(scnMgr->getRootSceneNode(), dynamicsWorld);
	dynamicsWorld->setDebugDrawer(dbgdraw);
	dbgdraw->setDebugMode(bulletDebugDraw);
}

SceneNode * Ground::getNode()
{
	return node;
}

btRigidBody * Ground::getBody()
{
	return body;
}

/*void Ground::update(float dt)
{
}

SceneNode * Ground::getNode()
{
	return node;
}
btRigidBody * Ground::getBody()
{
	return nullptr;
}
void Ground::setRotation(Vector3 axis, Radian angle)
{
}
void Ground::setup(SceneManager * scnMgr, btAlignedObjectArray<btCollisionShape*> collisionShapes, btDiscreteDynamicsWorld * dynamicsWorld)
{
	// Create a plane
	Plane plane(Vector3::UNIT_Y, 0);

	// Define the plane mesh
	MeshManager::getSingleton().createPlane(
		"ground", RGN_DEFAULT,
		plane,
		15000, 15000, 200, 200,
		true,
		1, 5, 5,
		Vector3::UNIT_Z);
	
	// Create an entity for the ground
	Entity* groundEntity = scnMgr->createEntity("ground");
	//Setup ground entity
	// Shadows off
	groundEntity->setCastShadows(false);

	// Material - Examples is the rsources file,
	// Rockwall (texture/properties) is defined inside it.
	groundEntity->setMaterialName("Examples/Rockwall");

	// Create a scene node to add the mesh too.
	SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	thisSceneNode->attachObject(groundEntity);

	//the ground is a cube of side 100 at position y = 0.
	//the sphere will hit it at y = -6, with center at -5
	btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(7500.), btScalar(500.), btScalar(7500.)));

	collisionShapes.push_back(groundShape);

	btTransform groundTransform;
	groundTransform.setIdentity();
	//  groundTransform.setOrigin(btVector3(0, -100, 0));

	Vector3 pos = thisSceneNode->_getDerivedPosition();

	//Box is 100 deep (dimensions are 1/2 heights)
	//but the plane position is flat.
	groundTransform.setOrigin(btVector3(pos.x, pos.y - 50.0, pos.z));

	Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
	groundTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));


	btScalar mass(0.);

	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
		groundShape->calculateLocalInertia(mass, localInertia);

	//using motionstate is optional, it provides interpolation capabilities, and only synchronizes 'active' objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
	btRigidBody* body = new btRigidBody(rbInfo);

	//   body->setRestitution(0.0);

	//add the body to the dynamics world
	dynamicsWorld->addRigidBody(body);



	/*mTerrainGlobals = OGRE_NEW Ogre::TerrainGlobalOptions();
	mTerrainGroup = OGRE_NEW Ogre::TerrainGroup(
		scnMgr,
		Ogre::Terrain::ALIGN_X_Z,
		513, 12000.0);
	mTerrainGroup->setFilenameConvention(Ogre::String("terrain"), Ogre::String("dat"));
	mTerrainGroup->setOrigin(Ogre::Vector3::ZERO);
	for (long x = 0; x <= 0; ++x)
		for (long y = 0; y <= 0; ++y)
			defineTerrain(x, y);

	mTerrainGroup->loadAllTerrains(true);
	if (mTerrainsImported)
	{
		Ogre::TerrainGroup::TerrainIterator ti = mTerrainGroup->getTerrainIterator();

		while (ti.hasMoreElements())
		{
			Ogre::Terrain* t = ti.getNext()->instance;
			initBlendMaps(t);
		}
	}
	mTerrainGroup->freeTemporaryResources();
}



void Ground::defineTerrain(long x, long y)
{
	Ogre::String filename = mTerrainGroup->generateFilename(x, y);

	bool exists =
		Ogre::ResourceGroupManager::getSingleton().resourceExists(
			mTerrainGroup->getResourceGroup(),
			filename);

	if (exists)
		mTerrainGroup->defineTerrain(x, y);
	else
	{
		Ogre::Image img;
		getTerrainImage(x % 2 != 0, y % 2 != 0, img);
		mTerrainGroup->defineTerrain(x, y, &img);

		mTerrainsImported = true;
	}
}*/

/*void Ground::initBlendMaps(Ogre::Terrain * terrain)
{
	Ogre::Real minHeight0 = 70;
	Ogre::Real fadeDist0 = 40;
	Ogre::Real minHeight1 = 70;
	Ogre::Real fadeDist1 = 15;

	Ogre::TerrainLayerBlendMap* blendMap0 = terrain->getLayerBlendMap(1);
	Ogre::TerrainLayerBlendMap* blendMap1 = terrain->getLayerBlendMap(2);

	float* pBlend0 = blendMap0->getBlendPointer();
	float* pBlend1 = blendMap1->getBlendPointer();

	for (Ogre::uint16 y = 0; y < terrain->getLayerBlendMapSize(); ++y)
	{
		for (Ogre::uint16 x = 0; x < terrain->getLayerBlendMapSize(); ++x)
		{
			Ogre::Real tx, ty;

			blendMap0->convertImageToTerrainSpace(x, y, &tx, &ty);
			Ogre::Real height = terrain->getHeightAtTerrainPosition(tx, ty);
			Ogre::Real val = (height - minHeight0) / fadeDist0;
			val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
			*pBlend0++ = val;

			val = (height - minHeight1) / fadeDist1;
			val = Ogre::Math::Clamp(val, (Ogre::Real)0, (Ogre::Real)1);
			*pBlend1++ = val;
		}
	}

	blendMap0->dirty();
	blendMap1->dirty();
	blendMap0->update();
	blendMap1->update();
}

void Ground::configureTerrainDefaults(SceneManager* scnMgr)
{
	mTerrainGlobals->setMaxPixelError(8);
	mTerrainGlobals->setCompositeMapDistance(3000);

	mTerrainGlobals->setLightMapDirection(scnMgr->getLight("DirectionalLight")->getDerivedDirection());
	mTerrainGlobals->setCompositeMapAmbient(scnMgr->getAmbientLight());
	mTerrainGlobals->setCompositeMapDiffuse(scnMgr->getLight("DirectionalLight")->getDiffuseColour());

	Ogre::Terrain::ImportData& importData = mTerrainGroup->getDefaultImportSettings();
	importData.terrainSize = 513;
	importData.worldSize = 12000.0;
	importData.inputScale = 600;
	importData.minBatchSize = 33;
	importData.maxBatchSize = 65;

	importData.layerList.resize(3);
	importData.layerList[0].worldSize = 100;
	importData.layerList[0].textureNames.push_back(
		"dirt_grayrocky_diffusespecular.dds");
	importData.layerList[0].textureNames.push_back(
		"dirt_grayrocky_normalheight.dds");
	importData.layerList[1].worldSize = 30;
	importData.layerList[1].textureNames.push_back(
		"grass_green-01_diffusespecular.dds");
	importData.layerList[1].textureNames.push_back(
		"grass_green-01_normalheight.dds");
	importData.layerList[2].worldSize = 200;
	importData.layerList[2].textureNames.push_back(
		"growth_weirdfungus-03_diffusespecular.dds");
	importData.layerList[2].textureNames.push_back(
		"growth_weirdfungus-03_normalheight.dds");
}

void Ground::getTerrainImage(bool flipX, bool flipY, Ogre::Image & img)
{
	img.load("terrain.png", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

	if (flipX)
		img.flipAroundY();
	if (flipY)
		img.flipAroundX();
}*/

