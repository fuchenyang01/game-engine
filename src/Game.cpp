/*-------------------------------------------------------------------------
Significant portions of this project are based on the Ogre Tutorials
- https://ogrecave.github.io/ogre/api/1.10/tutorials.html
Copyright (c) 2000-2013 Torus Knot Software Ltd

Manual generation of meshes from here:
- http://wiki.ogre3d.org/Generating+A+Mesh

*/

#include <exception>
#include <iostream>

#include "Game.h"
#include "Player.h"
#include "NPC.h"

Game Game::c_game;
Game::Game() : ApplicationContext("OgreTutorialApp")
{
    dynamicsWorld = NULL;
	forward = false;
	right = false;
	left = false;
	back = false;
}


Game::~Game()
{
    //cleanup in the reverse order of creation/initialization

    ///-----cleanup_start----
    //remove the rigidbodies from the dynamics world and delete them

    for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
    {
      btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
    	btRigidBody* body = btRigidBody::upcast(obj);

    	if (body && body->getMotionState())
    	{
    		delete body->getMotionState();
    	}

    	dynamicsWorld->removeCollisionObject(obj);
    	delete obj;
    }

    //delete collision shapes
    for (int j = 0; j < collisionShapes.size(); j++)
    {
    	btCollisionShape* shape = collisionShapes[j];
    	collisionShapes[j] = 0;
    	delete shape;
    }

    //delete dynamics world
    delete dynamicsWorld;

    //delete solver
    delete solver;

    //delete broadphase
    delete overlappingPairCache;

    //delete dispatcher
    delete dispatcher;

    delete collisionConfiguration;

    //next line is optional: it will be cleared by the destructor when the array goes out of scope
    collisionShapes.clear();
}


void Game::setup()
{
    // do not forget to call the base first
    ApplicationContext::setup();

    addInputListener(this);
	bulletInit();
    // get a pointer to the already created root
    Root* root = getRoot();
    scnMgr = root->createSceneManager();

    // register our scene with the RTSS
    RTShader::ShaderGenerator* shadergen = RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);

	player = GameObjectFactory::create("player");
	player->setup(scnMgr, collisionShapes, dynamicsWorld);

	ground = GameObjectFactory::create("ground");
	ground->setup(scnMgr, collisionShapes, dynamicsWorld);
	
	npc = GameObjectFactory::create("NPC");
	npc->setup(scnMgr, collisionShapes, dynamicsWorld);

	camera = GameObjectFactory::create("camera", scnMgr, getRenderWindow(),player->getNode());
   
	
    //setupCamera();

	

    setupLights();

	scnMgr->setSkyDome(true, "Examples/CloudySky", 5, 8);
	
	//Debug drawing
	player->dbgdraw = new BtOgre::DebugDrawer(scnMgr->getRootSceneNode(), dynamicsWorld);
	dynamicsWorld->setDebugDrawer(player->dbgdraw);
	player->dbgdraw->setDebugMode(player->bulletDebugDraw);
}



void Game::setupCamera()
{
   // Create Camera
  /*  Camera* cam = scnMgr->createCamera("myCam");

    //Setup Camera
    cam->setNearClipDistance(5);

    // Position Camera - to do this it must be attached to a scene graph and added
    // to the scene.
    SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    camNode->setPosition(0, 300, 600);
    camNode->lookAt(Vector3(0, 0, 0), Node::TransformSpace::TS_WORLD);
    camNode->attachObject(cam);

    // Setup viewport for the camera.
    Viewport* vp = getRenderWindow()->addViewport(cam);
    vp->setBackgroundColour(ColourValue(0, 0, 0));

    // link the camera and view port.
    cam->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));*/

}

void Game::bulletInit()
{
    ///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
	collisionConfiguration = new btDefaultCollisionConfiguration();

	///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
	dispatcher = new btCollisionDispatcher(collisionConfiguration);

	///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
	overlappingPairCache = new btDbvtBroadphase();

	///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	solver = new btSequentialImpulseConstraintSolver;

	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

	dynamicsWorld->setGravity(btVector3(0, -10, 0));
	
}




Game & Game::getInstance()
{
	return c_game;
}





bool Game::frameStarted (const Ogre::FrameEvent &evt)
{
  //Be sure to call base class - otherwise events are not polled.
	ApplicationContext::frameStarted(evt);
	Ogre::Vector3 d = player->getNode()->getPosition() - npc->getNode()->getPosition();
	d.normalise();
	fwd = btVector3(d.x, d.y, d.z);
  if (this->dynamicsWorld != NULL)
  {
      // Bullet can work with a fixed timestep
      //dynamicsWorld->stepSimulation(1.f / 60.f, 10);

      // Or a variable one, however, under the hood it uses a fixed timestep
      // then interpolates between them.
	  if (left) {
		  static_cast<Player*>(player)->left();
	  }
	  if (right) {
		  static_cast<Player*>(player)->right();
	  }
	  if (forward) {
		  static_cast<Player*>(player)->forward();
	  }
	  if (back) {
		  static_cast<Player*>(player)->back();
	  }

	  static_cast<NPC*>(npc)->track(fwd);

     dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);


     // update positions of all objects
     for (int j = dynamicsWorld->getNumCollisionObjects() - 1; j >= 0; j--)
     {
         btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[j];
         btRigidBody* body = btRigidBody::upcast(obj);
         btTransform trans;

         if (body && body->getMotionState())
         {
            body->getMotionState()->getWorldTransform(trans);

            /* https://oramind.com/ogre-bullet-a-beginners-basic-guide/ */
            void *userPointer = body->getUserPointer();

            // Player should know enough to update itself.
			if (userPointer == player) {
				player->update(evt.timeSinceLastEvent);
				
			}
             
            else //This is just to keep the other objects working.
            {
              if (userPointer)
              {
                btQuaternion orientation = trans.getRotation();
                Ogre::SceneNode *sceneNode = static_cast<Ogre::SceneNode *>(userPointer);
                sceneNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
                sceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
              }
            }
          }
          else
          {
            trans = obj->getWorldTransform();
          }
     }
	 static_cast<Player*>(player)->update((float)evt.timeSinceLastFrame);
	 npc->update(evt.timeSinceLastEvent);
	 player->dynamicsWorld->debugDrawWorld();
	

	 player->dbgdraw->step();
   }
  return true;
}

bool Game::frameEnded(const Ogre::FrameEvent &evt)
{
  if (this->dynamicsWorld != NULL)
  {
    // Bullet can work with a fixed timestep
    //dynamicsWorld->stepSimulation(1.f / 60.f, 10);

    // Or a variable one, however, under the hood it uses a fixed timestep
    // then interpolates between them.
	  

    dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);
  }
  return true;
}

void Game::setupLights()
{
    // Setup Abient light
    scnMgr->setAmbientLight(ColourValue(0, 0, 0));
    scnMgr->setShadowTechnique(ShadowTechnique::SHADOWTYPE_STENCIL_MODULATIVE);

    // Add a spotlight
    Light* spotLight = scnMgr->createLight("SpotLight");

    // Configure
    spotLight->setDiffuseColour(0, 0, 1.0);
    spotLight->setSpecularColour(0, 0, 1.0);
    spotLight->setType(Light::LT_SPOTLIGHT);
    spotLight->setSpotlightRange(Degree(35), Degree(50));


    // Create a schene node for the spotlight
    SceneNode* spotLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    spotLightNode->setDirection(0, -1, 0);
    spotLightNode->setPosition(Vector3(0, 600, 0));

    // Add spotlight to the scene node.
    spotLightNode->attachObject(spotLight);

    // Create directional light
    Light* directionalLight = scnMgr->createLight("DirectionalLight");

    // Configure the light
    directionalLight->setType(Light::LT_DIRECTIONAL);
    directionalLight->setDiffuseColour(ColourValue(1, 1, 1));
    directionalLight->setSpecularColour(ColourValue(1, 1, 1));

    // Setup a scene node for the directional lightnode.
    SceneNode* directionalLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    directionalLightNode->attachObject(directionalLight);
    directionalLightNode->setDirection(Vector3(0, -1, -1));

    // Create a point light
    Light* pointLight = scnMgr->createLight("PointLight");

    // Configure the light
    pointLight->setType(Light::LT_POINT);
    pointLight->setDiffuseColour(0.3, 0.3, 0.3);
    pointLight->setSpecularColour(0.3, 0.3, 0.3);

    // setup the scene node for the point light
    SceneNode* pointLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();

    // Configure the light
    pointLightNode->setPosition(Vector3(0, 600, 250));

    // Add the light to the scene.
    pointLightNode->attachObject(pointLight);

}

bool Game::keyPressed(const KeyboardEvent& evt)
{

    std::cout << "Got key event" << std::endl;
    if (evt.keysym.sym == SDLK_ESCAPE)
    {
        getRoot()->queueEndRendering();
    }

	if (evt.keysym.sym == SDLK_UP)
	{
		forward = true;
		
	}
	if (evt.keysym.sym == SDLK_LEFT)
	{
		
		left = true;
		
	}
	if (evt.keysym.sym == SDLK_RIGHT)
	{
	
		right = true;

	}
	if (evt.keysym.sym == SDLK_DOWN)
	{
		back = true;
	}
	if (evt.keysym.sym == 'd')
	{
		player->bulletDebugDraw = !player->bulletDebugDraw;
		player->dbgdraw->setDebugMode(player->bulletDebugDraw);
	}
    return true;
}

bool Game::keyReleased(const KeyboardEvent& evt)
{

	if (evt.keysym.sym == SDLK_UP)
	{
		forward = false;

	}
	if (evt.keysym.sym == SDLK_LEFT)
	{

		left = false;

	}
	if (evt.keysym.sym == SDLK_RIGHT)
	{

		right = false;

	}
	if (evt.keysym.sym == SDLK_DOWN)
	{
		back = false;
	}
	return true;

}


bool Game::mouseMoved(const MouseMotionEvent& evt)
{
	std::cout << "Got Mouse" << std::endl;
	return true;
}
